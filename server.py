#!/usr/bin/python3
import argparse

from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from http import HTTPStatus

from io import BytesIO


hostName = "localhost"
serverPort = 8000

request_counter = 0

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def __init__(self, fail_after, *args, **kwargs):
        self.fail_after = fail_after
        super().__init__(*args, **kwargs)

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(b"<html><head><title>Python Server</title></head>")
        self.wfile.write(b"<body>")
        self.wfile.write(bytes(f"<p>Request: {self.path}</p>", "utf-8"))
        self.wfile.write(b"</body></html>")

    def do_POST(self):
        global request_counter
        request_counter += 1
        response_body = None

        if self.fail_after and not request_counter % self.fail_after:
            self.send_response(500)
            self.end_headers()

            response_body = b'Error processing POST request'
        else:
            self.send_response(200)
            self.end_headers()

            response_body = b'POST request received\n\n'
            response_body += self.rfile.read(int(self.headers['Content-Length']))

        response = BytesIO()
        response.write(response_body)
        self.wfile.write(response.getvalue())

    def log_request(self, code='-', size='-'):
        if isinstance(code, HTTPStatus):
            code = code.value
        self.log_message('"%s" %s %s RC: %d', self.requestline, str(code), str(size), request_counter)


def createWebServer(args):
    handler = partial(SimpleHTTPRequestHandler, args.fail)
    webServer = HTTPServer((hostName, serverPort), handler)
    print(f"Server started - http://{hostName}:{serverPort}")

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--fail", type=int, help="Fail return after this number of calls", default=0, required=False)
    args = parser.parse_args()
    createWebServer(args)


if __name__ == "__main__":
    main()
