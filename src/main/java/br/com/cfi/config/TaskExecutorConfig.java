package br.com.cfi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class TaskExecutorConfig {

    private static final int MAX_POOL_SIZE = 20;
    private static final int CORE_POOL_SIZE = 10;
    private static final int QUEUE_CAPACITY = 10;

    @Bean
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        // maximum number of threads that can be created
        taskExecutor.setMaxPoolSize(MAX_POOL_SIZE);
        // minimum number of workers (parallel threads) to keep alive
        taskExecutor.setCorePoolSize(CORE_POOL_SIZE);
        // size of queue used when core pool is filled
        taskExecutor.setQueueCapacity(QUEUE_CAPACITY);
        taskExecutor.setThreadNamePrefix("SpringBatch-");
        // thread invokes itself on rejected pool
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return taskExecutor;
    }

}
