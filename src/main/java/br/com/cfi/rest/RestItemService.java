package br.com.cfi.rest;

import br.com.cfi.model.ControleMovimentacaoCliente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class RestItemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestItemService.class);

    private static final String API_SERVER_HOST = "localhost";
    private static final String API_SERVER_PORT = "8000";
    private static final String BASE_URL = "http://" + API_SERVER_HOST + ":" + API_SERVER_PORT + "/";

    private RestTemplate restTemplate;
    private URI uri;

    public RestItemService() {
        this.restTemplate = new RestTemplate();
        try {
            this.uri = new URI(BASE_URL);
        } catch (URISyntaxException e) {
            LOGGER.error("Erro construindo URL do serviço rest", e);
            System.exit(1);
        }
    }

    public boolean doPost(List<? extends ControleMovimentacaoCliente> items) {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<List<? extends ControleMovimentacaoCliente>> request = new HttpEntity<>(items, headers);
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
            return result.getStatusCode().is2xxSuccessful();
        } catch (HttpStatusCodeException ex) {
            LOGGER.warn("Erro ao fazer chamada rest: {} - {}", ex.getStatusCode(), ex.getResponseBodyAsString());
        } catch (RestClientException ex) {
            LOGGER.warn("Erro ao fazer chamada rest: {}", ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("Erro: {}", ex.getMessage());
        }
        return false;
    }

}
