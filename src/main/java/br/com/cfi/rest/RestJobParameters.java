package br.com.cfi.rest;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class RestJobParameters {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate dateQuery;

    public RestJobParameters(LocalDate dateQuery) {
        this.dateQuery = dateQuery;
    }

    public RestJobParameters() {
    }

    public LocalDate getDateQuery() {
        return dateQuery;
    }

    public void setDateQuery(LocalDate dateQuery) {
        this.dateQuery = dateQuery;
    }

    public long getDateQueryEpochDay() {
        if (dateQuery != null) {
            return dateQuery.toEpochDay();
        }
        return 0;
    }

}
