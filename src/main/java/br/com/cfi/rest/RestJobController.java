package br.com.cfi.rest;

import br.com.cfi.job.JobParameterKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/job")
public class RestJobController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestJobController.class);

    private JobLauncher jobLauncher;
    private Job job;

    @Autowired
    public RestJobController(JobLauncher jobLauncher, Job job) {
        this.jobLauncher = jobLauncher;
        this.job = job;
    }

    @PostMapping(path = "/load", consumes = "application/json", produces = "application/json")
    public BatchStatus loadJob(@RequestBody RestJobParameters restParams) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = newExecution(restParams);
        LOGGER.info("Executing job with parameters: " + jobParameters);

        JobExecution jobExecution = jobLauncher.run(job, jobParameters);

        LOGGER.info("JobExecution: " + jobExecution.getStatus());

        return jobExecution.getStatus();
    }

    private JobParameters newExecution(RestJobParameters restParams) {
        Map<String, JobParameter> parameters = new HashMap<>();
        parameters.put(JobParameterKey.EXECUTION_TIME.getKeyName(), new JobParameter(System.currentTimeMillis()));
        parameters.put(JobParameterKey.DATE_QUERY.getKeyName(), new JobParameter(restParams.getDateQueryEpochDay()));
        return new JobParameters(parameters);
    }

}
