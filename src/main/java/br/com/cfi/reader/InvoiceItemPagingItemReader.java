package br.com.cfi.reader;

import br.com.cfi.dto.InvoiceItemDTO;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class InvoiceItemPagingItemReader extends JdbcPagingItemReader<InvoiceItemDTO> {

    private static final int PAGE_SIZE = 100000;
    // formato sendo usado no java e oracle
    private static final String QUERY_DATE_FORMAT = "dd/MM/yyyy";

    public InvoiceItemPagingItemReader(DataSource dataSource, Long dateQueryEpochDay) throws Exception {
        super();
        setName("InvoiceItemPagingItemReader");
        setDataSource(dataSource);
        setPageSize(PAGE_SIZE);
        setQueryProvider(getQueryProvider(dataSource, dateQueryEpochDay));
        setRowMapper(getRowMapper());
        // precisa ser false quando o job é multi-thread
        setSaveState(false);
        // setMaxItemCount(200);
    }

    private PagingQueryProvider getQueryProvider(DataSource dataSource, Long dateQueryEpochDay) throws Exception {
        SqlPagingQueryProviderFactoryBean queryProvider = new SqlPagingQueryProviderFactoryBean();
        queryProvider.setDataSource(dataSource);
        queryProvider.setSelectClause("moc_cd_movimentacao, moc_dt_movimentacao, moc_dt_lancamento, moc_vl_lancamento, tpm_ds_tp_movimentacao");
        queryProvider.setFromClause("sa.movimentacao_cliente, sa.tipo_movimentacao");
        queryProvider.setWhereClause(getWhereClause(dateQueryEpochDay));
        queryProvider.setSortKey("moc_cd_movimentacao");
        return queryProvider.getObject();
    }

    private String getWhereClause(Long dateQueryEpochDay) {
        LocalDate dateQuery = LocalDate.ofEpochDay(dateQueryEpochDay);
        String dateQueryStr = dateQuery.format(DateTimeFormatter.ofPattern(QUERY_DATE_FORMAT));
        String dateCondition = String.format("to_char(moc_dt_movimentacao, '%s') = '%s'", QUERY_DATE_FORMAT, dateQueryStr);
        String notExistsQuery = "SELECT 1 FROM sa.controle_movimentacao_cliente WHERE id_movimentacao = SA.movimentacao_cliente.moc_cd_movimentacao and flg_processado = 1";
        return String.format("WHERE %s AND moc_cd_tipo_movimentacao = tpm_cd_tipo_movimentacao AND NOT EXISTS (%s)", dateCondition, notExistsQuery);
    }

    public RowMapper<InvoiceItemDTO> getRowMapper() {
        return (rs, rn) -> {
            InvoiceItemDTO e = new InvoiceItemDTO();
            e.setId(rs.getLong(1));
            e.setDataMovimentacao(rs.getDate(2).toLocalDate());
            e.setDataLancamento(rs.getDate(3).toLocalDate());
            e.setValorLancamento(rs.getDouble(4));
            e.setTipoMovimentacao(rs.getString(5));
            return e;
        };
    }

}
