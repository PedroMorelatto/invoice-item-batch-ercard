package br.com.cfi.reader;

import br.com.cfi.model.ControleMovimentacaoCliente;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ControleMovimentacaoClientePagingItemReader extends JdbcPagingItemReader<ControleMovimentacaoCliente> {

    private static final int PAGE_SIZE = 1000;
    private static final String QUERY_DATE_FORMAT = "dd/MM/yyyy";

    public ControleMovimentacaoClientePagingItemReader(DataSource dataSource, Long dateQueryEpochDay) throws Exception {
        super();
        setName("ControleMovimentacaoClientePagingItemReader");
        setDataSource(dataSource);
        setPageSize(PAGE_SIZE);
        setQueryProvider(getQueryProvider(dataSource, dateQueryEpochDay));
        setRowMapper(getRowMapper());
    }

    private PagingQueryProvider getQueryProvider(DataSource dataSource, Long dateQueryEpochDay) throws Exception {
        SqlPagingQueryProviderFactoryBean queryProvider = new SqlPagingQueryProviderFactoryBean();
        queryProvider.setDataSource(dataSource);
        queryProvider.setSelectClause("id_movimentacao, data_movimentacao, data_lancamento, valor_lancamento, tipo_movimentacao");
        queryProvider.setFromClause("sa.controle_movimentacao_cliente");
        queryProvider.setWhereClause(getWhereClause(dateQueryEpochDay));
        queryProvider.setSortKey("id_movimentacao");
        return queryProvider.getObject();
    }

    private String getWhereClause(Long dateQueryEpochDay) {
        LocalDate dateQuery = LocalDate.ofEpochDay(dateQueryEpochDay);
        String dateQueryStr = dateQuery.format(DateTimeFormatter.ofPattern(QUERY_DATE_FORMAT));
        String dateCondition = String.format("to_char(data_lancamento, '%s') = '%s'", QUERY_DATE_FORMAT, dateQueryStr);
        return String.format("WHERE %s AND flg_processado = 0", dateCondition);
    }

    public RowMapper<ControleMovimentacaoCliente> getRowMapper() {
        return (rs, rn) -> {
            ControleMovimentacaoCliente c = new ControleMovimentacaoCliente();
            c.setIdMovimentacao(rs.getLong(1));
            c.setDataMovimentacao(rs.getDate(2).toLocalDate());
            c.setDataLancamento(rs.getDate(3).toLocalDate());
            c.setValorLancamento(rs.getDouble(4));
            c.setTipoMovimentacao(rs.getString(5));
            c.setFlgProcessado(rs.getInt(6));
            c.setNewEntity(false);
            return c;
        };
    }

}
