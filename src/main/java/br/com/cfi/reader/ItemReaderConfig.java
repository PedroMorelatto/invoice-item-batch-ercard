package br.com.cfi.reader;

import br.com.cfi.dto.InvoiceItemDTO;
import br.com.cfi.job.JobParameterKey;
import br.com.cfi.model.ControleMovimentacaoCliente;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Map;

@Configuration
public class ItemReaderConfig {

    private DataSource dataSource;

    @Autowired
    public ItemReaderConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    @StepScope
    public JdbcPagingItemReader<InvoiceItemDTO> invoiceItemPagingItemReader(@Value("#{jobParameters}") Map<String, Object> jobParameters) throws Exception {
        Long dateQueryEpochDay = (Long) jobParameters.get(JobParameterKey.DATE_QUERY.getKeyName());
        return new InvoiceItemPagingItemReader(dataSource, dateQueryEpochDay);
    }

    @Bean
    @StepScope
    public JdbcPagingItemReader<ControleMovimentacaoCliente> controleMovimentacaoClientePagingItemReader(@Value("#{jobParameters}") Map<String, Object> jobParameters) throws Exception {
        Long dateQueryEpochDay = (Long) jobParameters.get(JobParameterKey.DATE_QUERY.getKeyName());
        return new ControleMovimentacaoClientePagingItemReader(dataSource, dateQueryEpochDay);
    }

}
