package br.com.cfi.reader;

import br.com.cfi.model.ControleMovimentacaoCliente;
import br.com.cfi.dto.InvoiceItemDTO;
import br.com.cfi.dto.MovimentacoesDTO;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;

public class MovimentacaoClientesReader implements ItemStreamReader<MovimentacoesDTO> {

    private static final int LOTE = 10;

    private JdbcPagingItemReader<InvoiceItemDTO> delegate;
    private InvoiceItemDTO invoiceItemDTOAtual;

    public MovimentacaoClientesReader(JdbcPagingItemReader<InvoiceItemDTO> delegate) {
        this.delegate = delegate;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        delegate.open(executionContext);
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
        delegate.update(executionContext);
    }

    @Override
    public void close() throws ItemStreamException {
        delegate.close();
    }

    @Override
    public synchronized MovimentacoesDTO read() throws Exception {
        if (invoiceItemDTOAtual == null) {
            invoiceItemDTOAtual = delegate.read();
        }

        MovimentacoesDTO movimentacoesDTO = null;
        InvoiceItemDTO invoiceItemDTO = invoiceItemDTOAtual;
        invoiceItemDTOAtual = null;

        if (invoiceItemDTO != null) {
            movimentacoesDTO = new MovimentacoesDTO();
            movimentacoesDTO.add(getClientMovementControl(invoiceItemDTO));

            while (peek() != null && movimentacoesDTO.getSize() < LOTE) {
                movimentacoesDTO.add(getClientMovementControl(invoiceItemDTOAtual));
            }
        }

        return movimentacoesDTO;
    }

    private InvoiceItemDTO peek() throws Exception {
        invoiceItemDTOAtual = delegate.read();
        return invoiceItemDTOAtual;
    }

    public ControleMovimentacaoCliente getClientMovementControl(InvoiceItemDTO invoiceItemDTO) {
        return new ControleMovimentacaoCliente(
                invoiceItemDTO.getId(),
                invoiceItemDTO.getDataMovimentacao(),
                invoiceItemDTO.getDataLancamento(),
                invoiceItemDTO.getValorLancamento(),
                invoiceItemDTO.getTipoMovimentacao()
        );
    }

}
