package br.com.cfi.step;

import br.com.cfi.dto.InvoiceItemDTO;
import br.com.cfi.model.ControleMovimentacaoCliente;
import br.com.cfi.model.ControleMovimentacaoClienteRepository;
import br.com.cfi.task.InvoiceItemVerificationStep;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

@Configuration
public class StepConfig {

    private static final int COMMIT_INTERVAL = 100;

    private StepBuilderFactory stepBuilderFactory;
    private ControleMovimentacaoClienteRepository controleMovimentacaoClienteRepository;

    @Autowired
    public StepConfig(StepBuilderFactory stepBuilderFactory,
                      ControleMovimentacaoClienteRepository controleMovimentacaoClienteRepository) {
        this.stepBuilderFactory = stepBuilderFactory;
        this.controleMovimentacaoClienteRepository = controleMovimentacaoClienteRepository;
    }

    @Bean
    public Step dbJobStep(JdbcPagingItemReader<InvoiceItemDTO> itemReader,
                          ItemProcessor<InvoiceItemDTO, ControleMovimentacaoCliente> itemProcessor,
                          ItemWriter<ControleMovimentacaoCliente> itemWriter,
                          TaskExecutor threadPoolTaskExecutor) {
        return stepBuilderFactory
                .get("InvoiceItemDbToRestStep")
                .<InvoiceItemDTO, ControleMovimentacaoCliente>chunk(COMMIT_INTERVAL)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .taskExecutor(threadPoolTaskExecutor)
                // sem isso não cria mais que 4 threads
                .throttleLimit(20)
                .build();
    }

    @Bean
    public Step dbVerificationStep() {
        return stepBuilderFactory
                .get("InvoiceItemProcessedVerificationStep")
                .tasklet(new InvoiceItemVerificationStep(controleMovimentacaoClienteRepository))
                .build();
    }

}
