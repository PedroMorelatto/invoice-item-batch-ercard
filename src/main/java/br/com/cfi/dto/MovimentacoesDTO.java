package br.com.cfi.dto;

import br.com.cfi.model.ControleMovimentacaoCliente;

import java.util.ArrayList;
import java.util.List;

public class MovimentacoesDTO {

    private List<ControleMovimentacaoCliente> movimentacaoClientes;

    public MovimentacoesDTO() {
        this.movimentacaoClientes = new ArrayList<>();
    }

    public MovimentacoesDTO(List<ControleMovimentacaoCliente> movimentacaoClientes) {
        this.movimentacaoClientes = movimentacaoClientes;
    }

    public List<ControleMovimentacaoCliente> getMovimentacaoClientes() {
        return movimentacaoClientes;
    }

    public void setMovimentacaoClientes(List<ControleMovimentacaoCliente> movimentacaoClientes) {
        this.movimentacaoClientes = movimentacaoClientes;
    }

    public int getSize() {
        if (this.movimentacaoClientes != null) {
            return this.movimentacaoClientes.size();
        }
        return 0;
    }

    public void add(ControleMovimentacaoCliente controleMovimentacaoCliente) {
        if (this.movimentacaoClientes == null) {
            this.movimentacaoClientes = new ArrayList<>();
        }
        this.movimentacaoClientes.add(controleMovimentacaoCliente);
    }

    public void setProcessed() {
        movimentacaoClientes.forEach(controle -> controle.setFlgProcessado(1));
    }

    @Override
    public String toString() {
        return "MovimentacoesDTO{movimentacaoClientes=" + movimentacaoClientes + "}";
    }

}
