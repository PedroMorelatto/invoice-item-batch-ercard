package br.com.cfi.dto;

import java.time.LocalDate;

public class InvoiceItemDTO {

    private Long id;
    private LocalDate dataMovimentacao;
    private LocalDate dataLancamento;
    private Double valorLancamento;
    private String tipoMovimentacao;

    public InvoiceItemDTO() {
    }

    public InvoiceItemDTO(Long id, LocalDate dataMovimentacao, LocalDate dataLancamento, Double valorLancamento, String tipoMovimentacao) {
        this.id = id;
        this.dataMovimentacao = dataMovimentacao;
        this.dataLancamento = dataLancamento;
        this.valorLancamento = valorLancamento;
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataMovimentacao() {
        return dataMovimentacao;
    }

    public void setDataMovimentacao(LocalDate dataMovimentacao) {
        this.dataMovimentacao = dataMovimentacao;
    }

    public LocalDate getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(LocalDate dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public Double getValorLancamento() {
        return valorLancamento;
    }

    public void setValorLancamento(Double valorLancamento) {
        this.valorLancamento = valorLancamento;
    }

    public String getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(String tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

}
