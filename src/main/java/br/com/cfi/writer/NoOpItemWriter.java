package br.com.cfi.writer;

import br.com.cfi.dto.MovimentacoesDTO;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class NoOpItemWriter implements ItemWriter<MovimentacoesDTO> {

    @Override
    public void write(List<? extends MovimentacoesDTO> items) throws Exception {
        // no-op
    }

}
