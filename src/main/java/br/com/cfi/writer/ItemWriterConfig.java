package br.com.cfi.writer;

import br.com.cfi.dto.MovimentacoesDTO;
import br.com.cfi.model.ControleMovimentacaoCliente;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class ItemWriterConfig {

    private DataSource dataSource;

    @Autowired
    public ItemWriterConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public ItemWriter<MovimentacoesDTO> itemWriter() {
        return new NoOpItemWriter();
    }

    @Bean
    public ItemWriter<ControleMovimentacaoCliente> controleMovimentacaoClienteItemWriter() {
        return new ControleMovimentacaoBatchItemWriter(dataSource);
    }

}
