package br.com.cfi.writer;

import br.com.cfi.dto.MovimentacoesDTO;
import br.com.cfi.model.ControleMovimentacaoCliente;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class MovimentacaoClientesWriter implements ItemWriter<MovimentacoesDTO> {

    private ItemWriter<ControleMovimentacaoCliente> delegate;

    public MovimentacaoClientesWriter(ItemWriter<ControleMovimentacaoCliente> delegate) {
        this.delegate = delegate;
    }

    @Override
    public void write(List<? extends MovimentacoesDTO> items) throws Exception {
        for (MovimentacoesDTO item : items) {
            delegate.write(item.getMovimentacaoClientes());
        }
    }

}
