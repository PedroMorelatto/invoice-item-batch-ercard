package br.com.cfi.writer;

import br.com.cfi.dto.MovimentacoesDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

public class RestItemWriter implements ItemWriter<MovimentacoesDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestItemWriter.class);

    private final RestTemplate restTemplate = new RestTemplate();

    private static final String API_SERVER_HOST = "localhost";
    private static final String API_SERVER_PORT = "8000";

    @Override
    public void write(List<? extends MovimentacoesDTO> items) throws Exception {
        final String baseUrl = "http://" + API_SERVER_HOST + ":" + API_SERVER_PORT + "/";
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        // headers.set("X-COM-LOCATION", "USA");

        for (MovimentacoesDTO item : items) {
            HttpEntity<MovimentacoesDTO> request = new HttpEntity<>(item, headers);
            ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
            LOGGER.info("result=" + result);
            if (!result.getStatusCode().is2xxSuccessful()) {
                throw new RuntimeException("");
            }
        }
    }

}