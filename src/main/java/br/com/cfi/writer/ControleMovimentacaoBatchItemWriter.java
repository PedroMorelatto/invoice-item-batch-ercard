package br.com.cfi.writer;

import br.com.cfi.model.ControleMovimentacaoCliente;
import br.com.cfi.rest.RestItemService;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;
import java.util.List;

public class ControleMovimentacaoBatchItemWriter extends JdbcBatchItemWriter<ControleMovimentacaoCliente> {

    private static final int LOTE = 10;

    private RestItemService restItemService;

    public ControleMovimentacaoBatchItemWriter(DataSource dataSource) {
        super();
        setDataSource(dataSource);
        setSql("INSERT INTO CONTROLE_MOVIMENTACAO_CLIENTE VALUES (:idMovimentacao, :dataMovimentacao, :dataLancamento, :valorLancamento, :tipoMovimentacao, :flgProcessado)");
        setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        afterPropertiesSet();
        this.restItemService = new RestItemService();
    }

    @Override
    public void write(List<? extends ControleMovimentacaoCliente> items) throws Exception {
        for (int i = 0; i < items.size(); i += LOTE) {
            int end = Math.min(items.size(), i + LOTE);
            List<? extends ControleMovimentacaoCliente> chunk = items.subList(i, end);
            boolean result = restItemService.doPost(chunk);
            chunk.forEach(item -> item.setFlgProcessado(result ? 1 : 0));
            super.write(chunk);
        }
    }

}
