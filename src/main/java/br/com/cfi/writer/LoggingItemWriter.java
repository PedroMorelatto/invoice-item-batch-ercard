package br.com.cfi.writer;

import br.com.cfi.dto.InvoiceItemDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;


public class LoggingItemWriter implements ItemWriter<InvoiceItemDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingItemWriter.class);

    @Override
    public void write(List<? extends InvoiceItemDTO> items) throws Exception {
        LOGGER.info("Writing users: {}", items);
    }
}