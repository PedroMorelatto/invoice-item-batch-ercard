package br.com.cfi.task;

import br.com.cfi.model.ControleMovimentacaoCliente;
import br.com.cfi.model.ControleMovimentacaoClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.time.LocalDate;
import java.util.List;

public class InvoiceItemVerificationStep implements Tasklet {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceItemVerificationStep.class);

    private ControleMovimentacaoClienteRepository controleMovimentacaoClienteRepository;

    public InvoiceItemVerificationStep(ControleMovimentacaoClienteRepository controleMovimentacaoClienteRepository) {
        this.controleMovimentacaoClienteRepository = controleMovimentacaoClienteRepository;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        Long dateQueryEpochDay = chunkContext.getStepContext().getStepExecution().getJobParameters().getLong("dateQuery");
        if (dateQueryEpochDay != null) {
            LocalDate dateQuery = LocalDate.ofEpochDay(dateQueryEpochDay);
            List<ControleMovimentacaoCliente> failed = controleMovimentacaoClienteRepository.findFailedFromDate(dateQuery);
            for (ControleMovimentacaoCliente controle : failed) {
                LOGGER.warn("Registro com erro: {}", controle);
            }
        }

        return RepeatStatus.FINISHED;
    }

}
