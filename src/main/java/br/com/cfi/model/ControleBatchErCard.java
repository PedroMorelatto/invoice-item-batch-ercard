package br.com.cfi.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class ControleBatchErCard implements Persistable<Long>, Serializable {

    @Id
    private Long idJob;
    private String jobStatus;
    private LocalDateTime executionTime;
    private LocalDate queryDate;
    @Transient
    private boolean newEntity;

    public ControleBatchErCard(Long idJob, String jobStatus, LocalDateTime executionTime, LocalDate queryDate) {
        this.idJob = idJob;
        this.jobStatus = jobStatus;
        this.executionTime = executionTime;
        this.queryDate = queryDate;
        this.newEntity = true;
    }

    public ControleBatchErCard(ControleBatchErCard lastJob) {
        this.idJob = lastJob.getIdJob() + 1;
        this.jobStatus = ControleBatchStatus.RUNNING.name();
        this.executionTime = LocalDateTime.now();
        this.queryDate = lastJob.getQueryDate().plusDays(1);
        this.newEntity = true;
    }

    public ControleBatchErCard() {
        this.idJob = 1L;
        this.jobStatus = ControleBatchStatus.RUNNING.name();
        this.executionTime = LocalDateTime.now();
        // this.queryDate = LocalDate.now().minusDays(1);
        this.queryDate = LocalDate.of(2021, 4, 29);
        this.newEntity = true;
    }

    @Override
    public Long getId() {
        return idJob;
    }

    @Override
    public boolean isNew() {
        return newEntity;
    }

    public Long getIdJob() {
        return idJob;
    }

    public void setIdJob(Long idJob) {
        this.idJob = idJob;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public LocalDateTime getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(LocalDateTime executionTime) {
        this.executionTime = executionTime;
    }

    public LocalDate getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(LocalDate queryDate) {
        this.queryDate = queryDate;
    }

    public void setNewEntity(boolean newEntity) {
        this.newEntity = newEntity;
    }

}
