package br.com.cfi.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ControleMovimentacaoClienteRepository extends CrudRepository<ControleMovimentacaoCliente, Long> {

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    <S extends ControleMovimentacaoCliente> Iterable<S> saveAll(Iterable<S> entities);

    List<ControleMovimentacaoCliente> findAllByDataLancamentoAndFlgProcessado(LocalDate dataLancamento, int flagProcessado);

    default List<ControleMovimentacaoCliente> findFailedFromDate(LocalDate dataLancamento) {
        return findAllByDataLancamentoAndFlgProcessado(dataLancamento, 0);
    }

}
