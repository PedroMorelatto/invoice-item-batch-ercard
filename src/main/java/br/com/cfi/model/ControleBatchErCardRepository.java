package br.com.cfi.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ControleBatchErCardRepository extends CrudRepository<ControleBatchErCard, Long> {

    ControleBatchErCard findTopByJobStatusOrderByExecutionTimeDesc(String jobStatus);

    default ControleBatchErCard findLastDoneJob(ControleBatchStatus jobStatus) {
        return findTopByJobStatusOrderByExecutionTimeDesc(jobStatus.name());
    }

    default ControleBatchErCard findCurrentJob() {
        return findTopByJobStatusOrderByExecutionTimeDesc(ControleBatchStatus.RUNNING.name());
    }

}
