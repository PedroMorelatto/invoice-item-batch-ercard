package br.com.cfi.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.time.LocalDate;

public class ControleMovimentacaoCliente implements Persistable<Long>, Serializable {

    @Id
    private Long idMovimentacao;
    private LocalDate dataMovimentacao;
    private LocalDate dataLancamento;
    private double valorLancamento;
    private String tipoMovimentacao;
    private int flgProcessado;
    @Transient
    private boolean newEntity;

    public ControleMovimentacaoCliente(Long idMovimentacao,
                                       LocalDate dataMovimentacao,
                                       LocalDate dataLancamento,
                                       double valorLancamento,
                                       String tipoMovimentacao) {
        this.idMovimentacao = idMovimentacao;
        this.dataMovimentacao = dataMovimentacao;
        this.dataLancamento = dataLancamento;
        this.valorLancamento = valorLancamento;
        this.tipoMovimentacao = tipoMovimentacao;
        this.flgProcessado = 0;
        this.newEntity = true;
    }

    public ControleMovimentacaoCliente() {
    }

    @Override
    public Long getId() {
        return idMovimentacao;
    }

    @Override
    public boolean isNew() {
        return newEntity;
    }

    public Long getIdMovimentacao() {
        return idMovimentacao;
    }

    public void setIdMovimentacao(Long idMovimentacao) {
        this.idMovimentacao = idMovimentacao;
    }

    public LocalDate getDataMovimentacao() {
        return dataMovimentacao;
    }

    public void setDataMovimentacao(LocalDate dataMovimentacao) {
        this.dataMovimentacao = dataMovimentacao;
    }

    public LocalDate getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(LocalDate dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public double getValorLancamento() {
        return valorLancamento;
    }

    public void setValorLancamento(double valorLancamento) {
        this.valorLancamento = valorLancamento;
    }

    public String getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(String tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public int getFlgProcessado() {
        return flgProcessado;
    }

    public void setFlgProcessado(int flgProcessado) {
        this.flgProcessado = flgProcessado;
    }

    public void setNewEntity(boolean newEntity) {
        this.newEntity = newEntity;
    }

    @Override
    public String toString() {
        return "ControleMovimentacaoCliente{" +
                "idMovimentacao=" + idMovimentacao +
                ", dataMovimentacao=" + dataMovimentacao +
                ", dataLancamento=" + dataLancamento +
                ", valorLancamento=" + valorLancamento +
                ", tipoMovimentacao='" + tipoMovimentacao + "'}";
    }

}
