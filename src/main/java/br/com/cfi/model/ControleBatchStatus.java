package br.com.cfi.model;

import org.springframework.batch.core.ExitStatus;

public enum ControleBatchStatus {
    RUNNING,
    COMPLETED,
    FAILED;

    public static ControleBatchStatus getFromExitStatus(ExitStatus exitStatus) {
        if (ExitStatus.EXECUTING.equals(exitStatus)) {
            return RUNNING;
        } else if (ExitStatus.COMPLETED.equals(exitStatus) || ExitStatus.NOOP.equals(exitStatus) || ExitStatus.STOPPED.equals(exitStatus)) {
            return COMPLETED;
        } else {
            // ExitStatus.UNKNOWN || ExitStatus.FAILED
            return FAILED;
        }
    }

}
