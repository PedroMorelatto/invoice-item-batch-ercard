package br.com.cfi.job;

public enum JobParameterKey {

    EXECUTION_TIME("executionTime"),
    DATE_QUERY("dateQuery");

    private final String keyName;

    JobParameterKey(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyName() {
        return keyName;
    }

}
