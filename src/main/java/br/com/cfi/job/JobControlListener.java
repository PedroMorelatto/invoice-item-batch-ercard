package br.com.cfi.job;

import br.com.cfi.model.ControleBatchErCard;
import br.com.cfi.model.ControleBatchErCardRepository;
import br.com.cfi.model.ControleBatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.item.ExecutionContext;

public class JobControlListener implements JobExecutionListener {

    private ControleBatchErCardRepository controleBatchErCardRepository;

    public JobControlListener(ControleBatchErCardRepository controleBatchErCardRepository) {
        this.controleBatchErCardRepository = controleBatchErCardRepository;
    }

    public void beforeJob(JobExecution jobExecution) {
        ControleBatchErCard lastJob = controleBatchErCardRepository.findLastDoneJob(ControleBatchStatus.COMPLETED);
        ControleBatchErCard newJob = lastJob != null ? new ControleBatchErCard(lastJob) : new ControleBatchErCard();
        controleBatchErCardRepository.save(newJob);

        ExecutionContext executionContext = jobExecution.getExecutionContext();
        // verificando para não sobrescrever quando passar por rest
        if (jobExecution.getJobParameters().getLong(JobParameterKey.DATE_QUERY.getKeyName()) == null) {
            executionContext.put(JobParameterKey.DATE_QUERY.getKeyName(), newJob.getQueryDate().toEpochDay());
        }
    }

    public void afterJob(JobExecution jobExecution) {
        ControleBatchErCard currentJob = controleBatchErCardRepository.findCurrentJob();
        currentJob.setJobStatus(ControleBatchStatus.getFromExitStatus(jobExecution.getExitStatus()).name());
        currentJob.setNewEntity(false);
        controleBatchErCardRepository.save(currentJob);
    }

}
