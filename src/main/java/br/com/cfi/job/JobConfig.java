package br.com.cfi.job;

import br.com.cfi.model.ControleBatchErCardRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobConfig {

    private JobBuilderFactory jobBuilderFactory;
    private ControleBatchErCardRepository controleBatchErCardRepository;

    @Autowired
    public JobConfig(JobBuilderFactory jobBuilderFactory, ControleBatchErCardRepository controleBatchErCardRepository) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.controleBatchErCardRepository = controleBatchErCardRepository;
    }

    @Bean
    public Job dbJob(Step dbJobStep, Step dbVerificationStep) {
        return jobBuilderFactory
                .get("InvoiceItemJob")
                .incrementer(new RunIdIncrementer())
                .start(dbJobStep)
                .next(dbVerificationStep)
                .listener(new JobControlListener(controleBatchErCardRepository))
                .build();
    }

}
