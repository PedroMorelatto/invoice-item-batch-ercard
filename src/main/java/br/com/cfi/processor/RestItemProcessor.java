package br.com.cfi.processor;

import br.com.cfi.dto.MovimentacoesDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

public class RestItemProcessor implements ItemProcessor<MovimentacoesDTO, MovimentacoesDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestItemProcessor.class);

    private static final String API_SERVER_HOST = "localhost";
    private static final String API_SERVER_PORT = "8000";
    private static final String BASE_URL = "http://" + API_SERVER_HOST + ":" + API_SERVER_PORT + "/";

    private RestTemplate restTemplate;
    private URI uri;

    public RestItemProcessor() {
        this.restTemplate = new RestTemplate();
        try {
            this.uri = new URI(BASE_URL);
        } catch (URISyntaxException e) {
            LOGGER.error("Erro construindo URL do serviço rest", e);
            System.exit(1);
        }
    }

    @Override
    public MovimentacoesDTO process(MovimentacoesDTO item) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<MovimentacoesDTO> request = new HttpEntity<>(item, headers);
        try {
            ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
            if (result.getStatusCode().is2xxSuccessful()) {
                item.setProcessed();
            }
        } catch (Exception ex) {
            LOGGER.warn("Erro fazendo chamada rest: {}", ex.getMessage());
        }
        return item;
    }

}
