package br.com.cfi.processor;

import br.com.cfi.dto.InvoiceItemDTO;
import br.com.cfi.model.ControleMovimentacaoCliente;
import br.com.cfi.dto.MovimentacoesDTO;
import br.com.cfi.model.ControleMovimentacaoClienteRepository;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ItemProcessorConfig {

    private ControleMovimentacaoClienteRepository controleMovimentacaoClienteRepository;

    @Autowired
    public ItemProcessorConfig(ControleMovimentacaoClienteRepository controleMovimentacaoClienteRepository) {
        this.controleMovimentacaoClienteRepository = controleMovimentacaoClienteRepository;
    }

    @Bean
    public ItemProcessor<InvoiceItemDTO, ControleMovimentacaoCliente> itemProcessor() {
        return new ControleMovimentacaoItemProcessor();
    }

    @Bean
    public ItemProcessListener<MovimentacoesDTO, MovimentacoesDTO> itemProcessListener() {
        return new RestItemProcessListener(controleMovimentacaoClienteRepository);
    }

}
