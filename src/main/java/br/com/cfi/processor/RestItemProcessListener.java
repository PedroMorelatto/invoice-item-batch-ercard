package br.com.cfi.processor;

import br.com.cfi.dto.MovimentacoesDTO;
import br.com.cfi.model.ControleMovimentacaoClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;

public class RestItemProcessListener implements ItemProcessListener<MovimentacoesDTO, MovimentacoesDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestItemProcessListener.class);

    private ControleMovimentacaoClienteRepository controleMovimentacaoRepository;

    public RestItemProcessListener(ControleMovimentacaoClienteRepository controleMovimentacaoRepository) {
        this.controleMovimentacaoRepository = controleMovimentacaoRepository;
    }

    @Override
    public void beforeProcess(MovimentacoesDTO item) {

    }

    @Override
    public void afterProcess(MovimentacoesDTO item, MovimentacoesDTO result) {
        if (result != null) {
            controleMovimentacaoRepository.saveAll(result.getMovimentacaoClientes());
        }
    }

    @Override
    public void onProcessError(MovimentacoesDTO item, Exception e) {
        // nunca deveria cair aqui por causa do try catch no processor
        LOGGER.error("Error processing items=" + item, e);
    }

}
