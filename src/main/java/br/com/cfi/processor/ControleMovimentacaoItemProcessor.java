package br.com.cfi.processor;

import br.com.cfi.dto.InvoiceItemDTO;
import br.com.cfi.model.ControleMovimentacaoCliente;
import org.springframework.batch.item.ItemProcessor;

public class ControleMovimentacaoItemProcessor implements ItemProcessor<InvoiceItemDTO, ControleMovimentacaoCliente> {

    @Override
    public ControleMovimentacaoCliente process(InvoiceItemDTO item) throws Exception {
        return new ControleMovimentacaoCliente(
                item.getId(),
                item.getDataMovimentacao(),
                item.getDataLancamento(),
                item.getValorLancamento(),
                item.getTipoMovimentacao()
        );
    }

}
